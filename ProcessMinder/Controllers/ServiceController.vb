﻿Module ServiceController

    Private services As New ArrayList

    Public Property Index As ArrayList
        Get
            Return services
        End Get
        Set(value As ArrayList)
            services = value
        End Set
    End Property


    Public Sub Create(ByRef name As String, ByRef opts As Hashtable)
        Dim newService As New Service(name, opts)
        services.Add(newService)
        Application.save()
    End Sub

    Public Sub Update(ByRef oldService As Service, ByRef newService As Service)
        services.Remove(oldService)
        services.Add(newService)
        Application.save()
    End Sub

    Public Sub DestroyAll()
        For Each service As Service In services
            services.Remove(services.IndexOf(service))
        Next
        Application.save()
    End Sub

    Public Sub Destroy(ByRef target As Service)
        services.Remove(target)
        Application.save()
    End Sub

    Public Function Find(ByRef name As String) As ArrayList
        Dim found As New ArrayList
        For Each service As Service In services
            If service.name() = name Then
                found.Add(service)
            End If
        Next
        Return found
    End Function

End Module
