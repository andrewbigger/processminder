﻿<System.Serializable()>
Public Class Service

    Private serviceName As String
    Private serviceStatus As Boolean = False

    Private serviceCommand As String
    Private serviceArgs As String
    Private serviceDesc As String
    Private serviceWindow As Boolean
    Private serviceStartup As Boolean
    Private serviceLogDir As String

    Private serviceIcon As Integer

    <NonSerialized()> Private serviceProcess As System.Diagnostics.Process

    Dim permanent As Boolean = True 'hard coded for now

    Public Property name As String
        Get
            Return serviceName
        End Get
        Set(value As String)
            serviceName = value
        End Set
    End Property

    Public Property command As String
        Get
            Return serviceCommand
        End Get
        Set(value As String)
            serviceCommand = value
        End Set
    End Property

    Public Property args As String
        Get
            Return serviceArgs
        End Get
        Set(value As String)
            serviceArgs = value
        End Set
    End Property

    Public Property description As String
        Get
            Return serviceDesc
        End Get
        Set(value As String)
            serviceDesc = value
        End Set
    End Property

    Public Property window As Boolean
        Get
            Return serviceWindow
        End Get
        Set(value As Boolean)
            serviceWindow = value
        End Set
    End Property

    Public Property startup As Boolean
        Get
            Return serviceStartup
        End Get
        Set(value As Boolean)
            serviceStartup = value
        End Set
    End Property

    Public Property icon As Integer
        Get
            Return serviceIcon
        End Get
        Set(value As Integer)
            serviceIcon = value
        End Set
    End Property

    Public ReadOnly Property status As Boolean
        Get
            Return serviceStatus
        End Get
    End Property

    Public Property logDir As String

    Public Sub New(serviceName As String, opts As Hashtable)
        name = serviceName
        command = opts("command")
        args = opts("arguments")
        description = opts("description")
        permanent = opts("permanent")
        window = opts("window")
        startup = opts("startup")
        icon = opts("icon")
        logDir = opts("logdir")
    End Sub

    Public Sub Start()
        Try

            Dim serviceProcessInfo As ProcessStartInfo = New ProcessStartInfo

            With serviceProcessInfo
                .Arguments = " " + If(permanent = "True", "/K", "/C") + " " + command + " " + args
                .FileName = "cmd.exe"

                If (window = False) Then
                    .WindowStyle = ProcessWindowStyle.Hidden
                End If

            End With

            serviceProcess = New System.Diagnostics.Process
            serviceProcess.StartInfo = serviceProcessInfo

            serviceProcess.Start()
            serviceStatus = True

        Catch e As Exception
            MessageBox.Show(
                "Process " + command + " could not be created because " + e.Message.ToString(),
                "ProcessMinder",
                MessageBoxButtons.OK,
                MessageBoxIcon.Error
                )
            serviceStatus = False
        End Try
    End Sub

    Public Sub OpenLog()
        Try
            Process.Start("explorer.exe", logDir)
        Catch ex As Exception
            MessageBox.Show(
                "Cannot open log directory because " + ex.Message,
                "ProcessMinder",
                MessageBoxButtons.OK,
                MessageBoxIcon.Error
                )
        End Try
    End Sub

    Public Sub Kill()
        With serviceProcess
            Try
                Dim target As System.Diagnostics.Process
                target = System.Diagnostics.Process.GetProcessById(.Id)
                target.Kill()
            Catch ex As Exception
                MessageBox.Show(
                    "Cannot kill process because " + ex.Message,
                    "ProcessMinder",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error
                    )
            End Try
        End With
    End Sub

End Class
