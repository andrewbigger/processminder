﻿Public Class MainView

    Private Sub MainView_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Application.boot()

        setToolbarStyle(Application.toolbarStyle)
        setProcessesViewStyle(Application.listStyle)
        Me.ShowInTaskbar = Application.taskbar
        If Application.show = False Then Me.Hide()

        setProcesses()
    End Sub

    Private Sub MainView_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing
        Me.Hide()
        e.Cancel = True
    End Sub

    Private Sub chkStartBar_CheckedChanged(sender As Object, e As EventArgs) Handles chkStartBar.CheckedChanged
        Me.ShowInTaskbar = Application.taskbar
    End Sub

    Private Sub LargeIconsToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles LargeIconsToolStripMenuItem.Click
        setProcessesViewStyle(View.LargeIcon)
    End Sub

    Private Sub DetailViewToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles DetailViewToolStripMenuItem.Click
        setProcessesViewStyle(View.Details)
    End Sub

    Private Sub SmallIconsToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SmallIconsToolStripMenuItem.Click
        setProcessesViewStyle(View.SmallIcon)
    End Sub

    Private Sub ListToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ListToolStripMenuItem.Click
        setProcessesViewStyle(View.List)
    End Sub

    Private Sub TileToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles TileToolStripMenuItem.Click
        setProcessesViewStyle(View.Tile)
    End Sub

    Private Sub SortAZToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SortAZToolStripMenuItem.Click
        setProcessOrder(True)
    End Sub

    Private Sub SortZAToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SortZAToolStripMenuItem.Click
        setProcessOrder(False)
    End Sub

    Private Sub IconsOnlyToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles IconsOnlyToolStripMenuItem.Click
        setToolbarStyle(ToolStripItemDisplayStyle.Image)
    End Sub

    Private Sub IconsAndTextToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles IconsAndTextToolStripMenuItem.Click
        setToolbarStyle(ToolStripItemDisplayStyle.ImageAndText)
    End Sub

    Private Sub TextOnlyToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles TextOnlyToolStripMenuItem.Click
        setToolbarStyle(ToolStripItemDisplayStyle.Text)
    End Sub

    Private Sub btnAddProcess_Click(sender As Object, e As EventArgs) Handles btnAddProcess.Click
        addProcess()
    End Sub

    Private Sub lstProcesses_DoubleClick(sender As Object, e As EventArgs) Handles lstProcesses.DoubleClick
        editProcess()
    End Sub

    Private Sub btnModify_Click(sender As Object, e As EventArgs) Handles btnModify.Click
        editProcess()
    End Sub

    Private Sub btnRemoveProcess_Click(sender As Object, e As EventArgs) Handles btnRemoveProcess.Click
        deleteProcess()
    End Sub

    Private Sub btnRun_Click(sender As Object, e As EventArgs) Handles btnRun.Click
        runProcess()
    End Sub

    Private Sub btnKill_Click(sender As Object, e As EventArgs) Handles btnKill.Click
        killProcess()
    End Sub

    Private Sub btnLog2_Click(sender As Object, e As EventArgs) Handles btnLog2.Click
        logProcess()
    End Sub

    Private Sub btnRun2_Click(sender As Object, e As EventArgs) Handles btnRun2.Click
        runProcess()
    End Sub

    Private Sub btnKill2_Click(sender As Object, e As EventArgs) Handles btnKill2.Click
        killProcess()
    End Sub

    Private Sub lstProcesses_toolbar_hilitie(sender As Object, e As EventArgs) Handles lstProcesses.MouseUp
        With lstProcesses
            Select Case .SelectedItems.Count
                Case 0
                    ableItemOptions(False)
                    setPreview(False)
                Case 1
                    ableItemOptions(True)
                    setPreview(True)
                Case Else
                    ableItemOptions(True)
                    Me.btnModify.Enabled = False
                    setPreview(False)
            End Select
        End With
    End Sub

    Private Sub ableItemOptions(ByVal enabled As Boolean)
        With Me
            .btnRemoveProcess.Enabled = enabled
            .btnModify.Enabled = enabled
            .btnRun.Enabled = enabled
            .btnKill.enabled = enabled
            .btnOpenLogDir.Enabled = enabled
        End With
    End Sub

    Private Sub setPreview(ByVal visible As Boolean)
        With Me
            .pnlPreview.Visible = visible

            If visible = True And .lstProcesses.SelectedItems.Count > 0 Then
                Dim selectedItem As ListViewItem = Me.lstProcesses.SelectedItems.Item(0)
                Dim selectedService As Service = ServiceController.Find(selectedItem.Text).Item(0)

                .lblPreviewName.Text = selectedService.name
                .lblIcon.ImageIndex = selectedService.icon

                .txtCommand.Text = selectedService.command + " " + selectedService.args
                .txtDescription.Text = selectedService.description
            End If

        End With
    End Sub

    Private Sub setToolbarStyle(ByRef style As ToolStripItemDisplayStyle)
        With Me
            .btnAddProcess.DisplayStyle = style
            .btnRemoveProcess.DisplayStyle = style
            .btnModify.DisplayStyle = style
            .btnRun.DisplayStyle = style
            .btnKill.DisplayStyle = style
            .btnView.DisplayStyle = style
            .btnOpenLogDir.DisplayStyle = style
        End With
        Application.toolbarStyle = style
    End Sub

    Private Sub setProcessesViewStyle(ByRef style As View)
        With Me
            .lstProcesses.View = style
        End With
        Application.listStyle = style
    End Sub

    Private Sub setProcessOrder(ByRef asc As Boolean)
        With Me
            If asc = True Then
                .lstProcesses.Sorting = Windows.Forms.SortOrder.Ascending
            Else
                .lstProcesses.Sorting = Windows.Forms.SortOrder.Descending
            End If
            .lstProcesses.Sort()
        End With
    End Sub

    Private Sub setProcesses()
        With Me
            .lstProcesses.Items.Clear()
            For Each service As Service In ServiceController.Index
                Dim newService As New ListViewItem(service.name)
                With newService
                    .Text = service.name
                    .SubItems.Add(service.description)
                    .ImageIndex = service.icon
                End With
                .lstProcesses.Items.Add(newService)
            Next
            setProcessOrder(Application.sortOrder)
        End With
    End Sub

    Private Sub addProcess()
        Dim action As New ServiceView
        With action
            .create = True
            .ShowDialog()
            If .DialogResult = Windows.Forms.DialogResult.OK Then setProcesses()
        End With
    End Sub

    Private Sub editProcess()
        If Me.lstProcesses.SelectedItems.Count > 0 Then
            Dim selectedItem As ListViewItem = Me.lstProcesses.SelectedItems.Item(0)

            If ServiceController.Find(selectedItem.Text).Count > 0 Then
                Dim selectedService As Service = ServiceController.Find(selectedItem.Text).Item(0)
                Dim action As New ServiceView
                With action
                    .create = False
                    .formService = selectedService
                    .ShowDialog()
                    If .DialogResult = Windows.Forms.DialogResult.OK Then setProcesses()
                End With
            End If
        End If
    End Sub

    Private Sub deleteProcess()
        For Each service As ListViewItem In lstProcesses.SelectedItems
            Dim selectedService As Service = ServiceController.Find(service.Text).Item(0)
            ServiceController.Destroy(selectedService)
        Next
        setProcesses()
        ableItemOptions(False)
    End Sub

    Private Sub runProcess()
        For Each Service As ListViewItem In lstProcesses.SelectedItems
            Dim selectedService As Service = ServiceController.Find(Service.Text).Item(0)
            selectedService.Start()
        Next
    End Sub

    Private Sub killProcess()
        For Each Service As ListViewItem In lstProcesses.SelectedItems
            Dim selectedService As Service = ServiceController.Find(Service.Text).Item(0)
            selectedService.Kill()
        Next
    End Sub

    Private Sub logProcess()
        For Each Service As ListViewItem In lstProcesses.SelectedItems
            Dim selectedService As Service = ServiceController.Find(Service.Text).Item(0)
            selectedService.OpenLog()
        Next
    End Sub

    Private Sub exportConfig()
        Dim browser As New SaveFileDialog

        With browser
            .Filter = ".dat|*.dat"
            .Title = "Save Processes"
            .ShowDialog()

            If .FileName <> Nothing And .CheckPathExists = True Then
                Try
                    IO.write(.FileName)

                    MessageBox.Show(
                        "Export completed successfully",
                        "ProcessMinder",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Information
                        )
                Catch e As Exception
                    MessageBox.Show(
                        "Export failed because " + e.Message.ToString(),
                        "ProcessMinder",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error
                        )
                End Try
            End If
        End With

    End Sub

    Private Sub importConfig()
        Dim browser As New OpenFileDialog

        With browser
            .Filter = ".dat|*.dat"
            .Title = "Import Processes"
            .ShowDialog()

            If .FileName <> Nothing And .CheckFileExists = True Then
                Try
                    IO.read(.FileName)
                    setProcesses()
                    Application.save()

                    MessageBox.Show(
                        "Import completed successfully",
                        "ProcessMinder",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Information
                        )

                Catch e As Exception
                    MessageBox.Show(
                        "Import failed because " + e.Message.ToString(),
                        "ProcessMinder",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error
                        )
                End Try
            End If

        End With

    End Sub

    Private Sub hideMain()
        Me.Hide()
    End Sub

    Private Sub showMain()
        Me.Show()
    End Sub

    Private Sub exitMain()
        Application.save()
        End
    End Sub

    Private Sub AddProcessToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles trayBtnAdd.Click
        addProcess()
    End Sub

    Private Sub ManageProcessesToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles trayBtnManage.Click
        showMain()
    End Sub

    Private Sub btnHide_Click_1(sender As Object, e As EventArgs)
        hideMain()
    End Sub

    Private Sub ExitToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles TrayExit.Click
        exitMain()
    End Sub

    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        exitMain()
    End Sub

    Private Sub tray_MouseDoubleClick(sender As Object, e As MouseEventArgs) Handles tray.MouseClick
        showMain()
    End Sub

    Private Sub btnOpenLogDir_Click(sender As Object, e As EventArgs) Handles btnOpenLogDir.Click
        logProcess()
    End Sub

    Private Sub btnExportConfig_Click(sender As Object, e As EventArgs) Handles btnExportConfig.Click
        exportConfig()
    End Sub

    Private Sub btnImportConfig_Click(sender As Object, e As EventArgs) Handles btnImportConfig.Click
        importConfig()
    End Sub

End Class