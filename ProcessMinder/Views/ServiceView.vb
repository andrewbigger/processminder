﻿Imports System.Windows.Forms

Public Class ServiceView

    Private createService As Boolean
    Private activeService As Service

    Public Property create As Boolean
        Get
            Return createService
        End Get
        Set(value As Boolean)
            createService = value
        End Set
    End Property

    Public Property formService As Service
        Get
            Return activeService
        End Get
        Set(value As Service)
            activeService = value
        End Set
    End Property

    Private Sub ServiceView_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If createService = False Then
            setWindow()
            Me.btnRunNow.Enabled = True
            Me.Text = activeService.name + " Service"
        Else
            Me.cmbIcon.SelectedItem = cmbIcon.Items.Item(0) ' set default icon
            Me.btnRunNow.Enabled = False
            Me.Text = "Create New Service"
        End If
    End Sub

    Private Function opts() As Hashtable
        Dim options As New Hashtable
        With Me
            options("description") = .txtDescription.Text
            options("command") = .txtCommand.Text
            options("arguments") = .txtArgs.Text
            options("permanent") = True
            options("window") = .chkShowProcess.Checked
            options("startup") = .chkRunAtStartup.Checked
            options("icon") = cmbIcon.SelectedIndex
            options("logdir") = .txtLogDir.Text
        End With
        Return options
    End Function

    Private Sub setWindow()
        With Me
            .txtProcessName.Text = activeService.name
            .txtCommand.Text = activeService.command
            .txtArgs.Text = activeService.args
            .txtDescription.Text = activeService.description
            .chkShowProcess.Checked = activeService.window
            .chkRunAtStartup.Checked = activeService.startup
            .cmbIcon.Text = cmbIcon.Items.Item(activeService.icon)
            .txtLogDir.Text = activeService.logDir
        End With
    End Sub

    Private Sub OK_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        With Me
            If isValid() = True Then

                If create.Equals(True) Then
                    ServiceController.Create(.txtProcessName.Text, opts())
                Else
                    Dim updatedService As New Service(.txtProcessName.Text, opts())
                    ServiceController.Update(activeService, updatedService)
                End If

                .DialogResult = System.Windows.Forms.DialogResult.OK
                .Close()
            End If
        End With
    End Sub

    Private Sub Cancel_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub btnRunNow_Click(sender As Object, e As EventArgs) Handles btnRunNow.Click
        activeService.Start()
    End Sub

    Private Sub btnBrowse_Click(sender As Object, e As EventArgs) Handles btnBrowse.Click
        Dim browser As New FolderBrowserDialog
        
        With browser
            .ShowDialog()
            Me.txtLogDir.Text = .SelectedPath
        End With

    End Sub

    Private Sub btnOpen_Click(sender As Object, e As EventArgs) Handles btnOpen.Click
        If Me.txtLogDir.Text = "" Then
            MessageBox.Show("You haven't selected a logging directory yet. Please choose a log directory and try again",
                            "ProcessMinder Error",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Error)
        Else
            activeService.OpenLog()
        End If
    End Sub

    Private Function isValid() As Boolean
        Dim valid As Boolean = True
        Dim message As String = ""

        With Me

            If valid = True And .txtProcessName.Text.Trim = "" Then
                valid = False
                message = "Service must have a name"
            End If

            If valid = True And .txtCommand.Text.Trim = "" Then
                valid = False
                message = "Service must have a command"
            End If

        End With

        If valid = False Then
            MessageBox.Show("Cannot create service because: " + message,
                            "ProcessMinder Error",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Error)
        End If

        Return valid
    End Function


    Private Sub btnKillProcess_Click(sender As Object, e As EventArgs) Handles btnKillProcess.Click
        activeService.Kill()
    End Sub
End Class
