﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ServiceView
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ServiceView))
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.btnRunNow = New System.Windows.Forms.Button()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.pnlTitle = New System.Windows.Forms.Panel()
        Me.pnlAppTitle = New System.Windows.Forms.Label()
        Me.txtArgs = New System.Windows.Forms.TextBox()
        Me.txtProcessName = New System.Windows.Forms.TextBox()
        Me.txtCommand = New System.Windows.Forms.TextBox()
        Me.lblName = New System.Windows.Forms.Label()
        Me.lblCommand = New System.Windows.Forms.Label()
        Me.chkShowProcess = New System.Windows.Forms.CheckBox()
        Me.lblArgs = New System.Windows.Forms.Label()
        Me.lblDescription = New System.Windows.Forms.Label()
        Me.txtDescription = New System.Windows.Forms.TextBox()
        Me.lblIcon = New System.Windows.Forms.Label()
        Me.cmbIcon = New System.Windows.Forms.ComboBox()
        Me.chkRunAtStartup = New System.Windows.Forms.CheckBox()
        Me.txtLogDir = New System.Windows.Forms.TextBox()
        Me.lblLogDir = New System.Windows.Forms.Label()
        Me.btnOpen = New System.Windows.Forms.Button()
        Me.btnBrowse = New System.Windows.Forms.Button()
        Me.btnKillProcess = New System.Windows.Forms.Button()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.pnlTitle.SuspendLayout()
        Me.SuspendLayout()
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TableLayoutPanel1.ColumnCount = 3
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 64.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.btnRunNow, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.btnSave, 2, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.btnCancel, 1, 0)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(341, 281)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 1
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(216, 29)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'btnRunNow
        '
        Me.btnRunNow.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnRunNow.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnRunNow.Location = New System.Drawing.Point(4, 3)
        Me.btnRunNow.Name = "btnRunNow"
        Me.btnRunNow.Size = New System.Drawing.Size(67, 23)
        Me.btnRunNow.TabIndex = 2
        Me.btnRunNow.Text = "Run Now"
        '
        'btnSave
        '
        Me.btnSave.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnSave.Location = New System.Drawing.Point(155, 3)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(58, 23)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "Save"
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(80, 3)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(67, 23)
        Me.btnCancel.TabIndex = 1
        Me.btnCancel.Text = "Cancel"
        '
        'pnlTitle
        '
        Me.pnlTitle.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.pnlTitle.Controls.Add(Me.pnlAppTitle)
        Me.pnlTitle.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlTitle.Location = New System.Drawing.Point(0, 0)
        Me.pnlTitle.Name = "pnlTitle"
        Me.pnlTitle.Size = New System.Drawing.Size(569, 33)
        Me.pnlTitle.TabIndex = 1
        '
        'pnlAppTitle
        '
        Me.pnlAppTitle.AutoSize = True
        Me.pnlAppTitle.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlAppTitle.ForeColor = System.Drawing.Color.White
        Me.pnlAppTitle.Location = New System.Drawing.Point(12, 9)
        Me.pnlAppTitle.Name = "pnlAppTitle"
        Me.pnlAppTitle.Size = New System.Drawing.Size(72, 18)
        Me.pnlAppTitle.TabIndex = 2
        Me.pnlAppTitle.Text = "Properties"
        '
        'txtArgs
        '
        Me.txtArgs.Location = New System.Drawing.Point(75, 97)
        Me.txtArgs.Name = "txtArgs"
        Me.txtArgs.Size = New System.Drawing.Size(479, 20)
        Me.txtArgs.TabIndex = 15
        '
        'txtProcessName
        '
        Me.txtProcessName.Location = New System.Drawing.Point(75, 45)
        Me.txtProcessName.Name = "txtProcessName"
        Me.txtProcessName.Size = New System.Drawing.Size(479, 20)
        Me.txtProcessName.TabIndex = 13
        '
        'txtCommand
        '
        Me.txtCommand.Location = New System.Drawing.Point(75, 71)
        Me.txtCommand.Name = "txtCommand"
        Me.txtCommand.Size = New System.Drawing.Size(385, 20)
        Me.txtCommand.TabIndex = 14
        '
        'lblName
        '
        Me.lblName.AutoSize = True
        Me.lblName.Location = New System.Drawing.Point(31, 48)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(38, 13)
        Me.lblName.TabIndex = 9
        Me.lblName.Text = "Name:"
        '
        'lblCommand
        '
        Me.lblCommand.AutoSize = True
        Me.lblCommand.Location = New System.Drawing.Point(12, 74)
        Me.lblCommand.Name = "lblCommand"
        Me.lblCommand.Size = New System.Drawing.Size(57, 13)
        Me.lblCommand.TabIndex = 10
        Me.lblCommand.Text = "Command:"
        '
        'chkShowProcess
        '
        Me.chkShowProcess.AutoSize = True
        Me.chkShowProcess.Location = New System.Drawing.Point(466, 73)
        Me.chkShowProcess.Name = "chkShowProcess"
        Me.chkShowProcess.Size = New System.Drawing.Size(94, 17)
        Me.chkShowProcess.TabIndex = 12
        Me.chkShowProcess.Text = "Show Process"
        Me.chkShowProcess.UseVisualStyleBackColor = True
        '
        'lblArgs
        '
        Me.lblArgs.AutoSize = True
        Me.lblArgs.Location = New System.Drawing.Point(38, 100)
        Me.lblArgs.Name = "lblArgs"
        Me.lblArgs.Size = New System.Drawing.Size(31, 13)
        Me.lblArgs.TabIndex = 11
        Me.lblArgs.Text = "Args:"
        '
        'lblDescription
        '
        Me.lblDescription.AutoSize = True
        Me.lblDescription.Location = New System.Drawing.Point(6, 126)
        Me.lblDescription.Name = "lblDescription"
        Me.lblDescription.Size = New System.Drawing.Size(63, 13)
        Me.lblDescription.TabIndex = 17
        Me.lblDescription.Text = "Description:"
        '
        'txtDescription
        '
        Me.txtDescription.Location = New System.Drawing.Point(75, 123)
        Me.txtDescription.Multiline = True
        Me.txtDescription.Name = "txtDescription"
        Me.txtDescription.Size = New System.Drawing.Size(479, 80)
        Me.txtDescription.TabIndex = 18
        '
        'lblIcon
        '
        Me.lblIcon.AutoSize = True
        Me.lblIcon.Location = New System.Drawing.Point(38, 212)
        Me.lblIcon.Name = "lblIcon"
        Me.lblIcon.Size = New System.Drawing.Size(31, 13)
        Me.lblIcon.TabIndex = 19
        Me.lblIcon.Text = "Icon:"
        '
        'cmbIcon
        '
        Me.cmbIcon.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbIcon.FormattingEnabled = True
        Me.cmbIcon.Items.AddRange(New Object() {"Disc Burn", "Disc", "Digital", "Ruby", "Recycle", "Unknown", "XML", "Archive", "Dos Exec", "XHTML/XML", "Sickbeard"})
        Me.cmbIcon.Location = New System.Drawing.Point(75, 209)
        Me.cmbIcon.Name = "cmbIcon"
        Me.cmbIcon.Size = New System.Drawing.Size(379, 21)
        Me.cmbIcon.TabIndex = 20
        '
        'chkRunAtStartup
        '
        Me.chkRunAtStartup.AutoSize = True
        Me.chkRunAtStartup.Location = New System.Drawing.Point(460, 211)
        Me.chkRunAtStartup.Name = "chkRunAtStartup"
        Me.chkRunAtStartup.Size = New System.Drawing.Size(95, 17)
        Me.chkRunAtStartup.TabIndex = 21
        Me.chkRunAtStartup.Text = "Run at Startup"
        Me.chkRunAtStartup.UseVisualStyleBackColor = True
        '
        'txtLogDir
        '
        Me.txtLogDir.Location = New System.Drawing.Point(76, 238)
        Me.txtLogDir.Name = "txtLogDir"
        Me.txtLogDir.ReadOnly = True
        Me.txtLogDir.Size = New System.Drawing.Size(316, 20)
        Me.txtLogDir.TabIndex = 23
        '
        'lblLogDir
        '
        Me.lblLogDir.AutoSize = True
        Me.lblLogDir.Location = New System.Drawing.Point(26, 241)
        Me.lblLogDir.Name = "lblLogDir"
        Me.lblLogDir.Size = New System.Drawing.Size(44, 13)
        Me.lblLogDir.TabIndex = 22
        Me.lblLogDir.Text = "Log Dir:"
        '
        'btnOpen
        '
        Me.btnOpen.Location = New System.Drawing.Point(479, 236)
        Me.btnOpen.Name = "btnOpen"
        Me.btnOpen.Size = New System.Drawing.Size(75, 23)
        Me.btnOpen.TabIndex = 24
        Me.btnOpen.Text = "Open"
        Me.btnOpen.UseVisualStyleBackColor = True
        '
        'btnBrowse
        '
        Me.btnBrowse.Location = New System.Drawing.Point(398, 236)
        Me.btnBrowse.Name = "btnBrowse"
        Me.btnBrowse.Size = New System.Drawing.Size(75, 23)
        Me.btnBrowse.TabIndex = 25
        Me.btnBrowse.Text = "Browse"
        Me.btnBrowse.UseVisualStyleBackColor = True
        '
        'btnKillProcess
        '
        Me.btnKillProcess.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnKillProcess.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnKillProcess.Location = New System.Drawing.Point(12, 284)
        Me.btnKillProcess.Name = "btnKillProcess"
        Me.btnKillProcess.Size = New System.Drawing.Size(86, 23)
        Me.btnKillProcess.TabIndex = 26
        Me.btnKillProcess.Text = "Kill Process"
        '
        'ServiceView
        '
        Me.AcceptButton = Me.btnSave
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = New System.Drawing.Size(569, 322)
        Me.Controls.Add(Me.btnKillProcess)
        Me.Controls.Add(Me.btnBrowse)
        Me.Controls.Add(Me.btnOpen)
        Me.Controls.Add(Me.txtLogDir)
        Me.Controls.Add(Me.lblLogDir)
        Me.Controls.Add(Me.chkRunAtStartup)
        Me.Controls.Add(Me.cmbIcon)
        Me.Controls.Add(Me.lblIcon)
        Me.Controls.Add(Me.txtDescription)
        Me.Controls.Add(Me.lblDescription)
        Me.Controls.Add(Me.txtArgs)
        Me.Controls.Add(Me.txtProcessName)
        Me.Controls.Add(Me.txtCommand)
        Me.Controls.Add(Me.lblName)
        Me.Controls.Add(Me.lblCommand)
        Me.Controls.Add(Me.chkShowProcess)
        Me.Controls.Add(Me.lblArgs)
        Me.Controls.Add(Me.pnlTitle)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "ServiceView"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Service"
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.pnlTitle.ResumeLayout(False)
        Me.pnlTitle.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents pnlTitle As System.Windows.Forms.Panel
    Friend WithEvents pnlAppTitle As System.Windows.Forms.Label
    Friend WithEvents txtArgs As System.Windows.Forms.TextBox
    Friend WithEvents txtProcessName As System.Windows.Forms.TextBox
    Friend WithEvents txtCommand As System.Windows.Forms.TextBox
    Friend WithEvents lblName As System.Windows.Forms.Label
    Friend WithEvents lblCommand As System.Windows.Forms.Label
    Friend WithEvents chkShowProcess As System.Windows.Forms.CheckBox
    Friend WithEvents lblArgs As System.Windows.Forms.Label
    Friend WithEvents lblDescription As System.Windows.Forms.Label
    Friend WithEvents txtDescription As System.Windows.Forms.TextBox
    Friend WithEvents lblIcon As System.Windows.Forms.Label
    Friend WithEvents cmbIcon As System.Windows.Forms.ComboBox
    Friend WithEvents btnRunNow As System.Windows.Forms.Button
    Friend WithEvents chkRunAtStartup As System.Windows.Forms.CheckBox
    Friend WithEvents txtLogDir As System.Windows.Forms.TextBox
    Friend WithEvents lblLogDir As System.Windows.Forms.Label
    Friend WithEvents btnOpen As System.Windows.Forms.Button
    Friend WithEvents btnBrowse As System.Windows.Forms.Button
    Friend WithEvents btnKillProcess As System.Windows.Forms.Button

End Class
