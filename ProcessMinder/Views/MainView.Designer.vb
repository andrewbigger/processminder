﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MainView
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MainView))
        Me.pnlTitle = New System.Windows.Forms.Panel()
        Me.pnlAppTitle = New System.Windows.Forms.Label()
        Me.pctMap = New System.Windows.Forms.PictureBox()
        Me.tabImageList = New System.Windows.Forms.ImageList(Me.components)
        Me.lstSmallIcons = New System.Windows.Forms.ImageList(Me.components)
        Me.tabSettings = New System.Windows.Forms.TabPage()
        Me.grpAgent = New System.Windows.Forms.GroupBox()
        Me.btnImportConfig = New System.Windows.Forms.Button()
        Me.btnExportConfig = New System.Windows.Forms.Button()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.chkShowAgent = New System.Windows.Forms.CheckBox()
        Me.chkStartBar = New System.Windows.Forms.CheckBox()
        Me.grpAbout = New System.Windows.Forms.GroupBox()
        Me.LinkLabel1 = New System.Windows.Forms.LinkLabel()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.tabProcesses = New System.Windows.Forms.TabPage()
        Me.lstProcesses = New System.Windows.Forms.ListView()
        Me.colName = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.colDescription = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lstLargeIcons = New System.Windows.Forms.ImageList(Me.components)
        Me.pnlInfo = New System.Windows.Forms.Panel()
        Me.pnlPreview = New System.Windows.Forms.Panel()
        Me.tabPreview = New System.Windows.Forms.TabControl()
        Me.tabDetails = New System.Windows.Forms.TabPage()
        Me.btnKill2 = New System.Windows.Forms.Button()
        Me.btnRun2 = New System.Windows.Forms.Button()
        Me.btnLog2 = New System.Windows.Forms.Button()
        Me.txtCommand = New System.Windows.Forms.TextBox()
        Me.lblCommand = New System.Windows.Forms.Label()
        Me.tabDescription = New System.Windows.Forms.TabPage()
        Me.txtDescription = New System.Windows.Forms.TextBox()
        Me.lblPreviewName = New System.Windows.Forms.Label()
        Me.lblDescriptionHeading = New System.Windows.Forms.Label()
        Me.lblDescription = New System.Windows.Forms.Label()
        Me.tolBootstrap = New System.Windows.Forms.ToolStrip()
        Me.btnAddProcess = New System.Windows.Forms.ToolStripButton()
        Me.btnRemoveProcess = New System.Windows.Forms.ToolStripButton()
        Me.btnModify = New System.Windows.Forms.ToolStripButton()
        Me.btnRun = New System.Windows.Forms.ToolStripButton()
        Me.btnKill = New System.Windows.Forms.ToolStripButton()
        Me.btnView = New System.Windows.Forms.ToolStripDropDownButton()
        Me.ToolbarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.IconsOnlyToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.IconsAndTextToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TextOnlyToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ProcessListToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DetailViewToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LargeIconsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SmallIconsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.SortAZToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SortZAToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.btnOpenLogDir = New System.Windows.Forms.ToolStripButton()
        Me.tabs = New System.Windows.Forms.TabControl()
        Me.tray = New System.Windows.Forms.NotifyIcon(Me.components)
        Me.trayMenu = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.trayBtnAdd = New System.Windows.Forms.ToolStripMenuItem()
        Me.trayBtnManage = New System.Windows.Forms.ToolStripMenuItem()
        Me.TrayExit = New System.Windows.Forms.ToolStripMenuItem()
        Me.lblIcon = New System.Windows.Forms.Label()
        Me.ProcessTabSplit = New System.Windows.Forms.SplitContainer()
        Me.pnlTitle.SuspendLayout()
        CType(Me.pctMap, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabSettings.SuspendLayout()
        Me.grpAgent.SuspendLayout()
        Me.grpAbout.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabProcesses.SuspendLayout()
        Me.pnlInfo.SuspendLayout()
        Me.pnlPreview.SuspendLayout()
        Me.tabPreview.SuspendLayout()
        Me.tabDetails.SuspendLayout()
        Me.tabDescription.SuspendLayout()
        Me.tolBootstrap.SuspendLayout()
        Me.tabs.SuspendLayout()
        Me.trayMenu.SuspendLayout()
        CType(Me.ProcessTabSplit, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ProcessTabSplit.Panel1.SuspendLayout()
        Me.ProcessTabSplit.Panel2.SuspendLayout()
        Me.ProcessTabSplit.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlTitle
        '
        Me.pnlTitle.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.pnlTitle.Controls.Add(Me.pnlAppTitle)
        Me.pnlTitle.Controls.Add(Me.pctMap)
        Me.pnlTitle.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlTitle.Location = New System.Drawing.Point(0, 0)
        Me.pnlTitle.Name = "pnlTitle"
        Me.pnlTitle.Size = New System.Drawing.Size(716, 65)
        Me.pnlTitle.TabIndex = 0
        '
        'pnlAppTitle
        '
        Me.pnlAppTitle.AutoSize = True
        Me.pnlAppTitle.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlAppTitle.ForeColor = System.Drawing.Color.White
        Me.pnlAppTitle.Location = New System.Drawing.Point(81, 25)
        Me.pnlAppTitle.Name = "pnlAppTitle"
        Me.pnlAppTitle.Size = New System.Drawing.Size(103, 18)
        Me.pnlAppTitle.TabIndex = 2
        Me.pnlAppTitle.Text = "Process Minder"
        '
        'pctMap
        '
        Me.pctMap.Image = Global.ProcessMinder.My.Resources.Resources.ico_mind
        Me.pctMap.Location = New System.Drawing.Point(-17, -10)
        Me.pctMap.Name = "pctMap"
        Me.pctMap.Size = New System.Drawing.Size(92, 80)
        Me.pctMap.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pctMap.TabIndex = 0
        Me.pctMap.TabStop = False
        '
        'tabImageList
        '
        Me.tabImageList.ImageStream = CType(resources.GetObject("tabImageList.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.tabImageList.TransparentColor = System.Drawing.Color.Transparent
        Me.tabImageList.Images.SetKeyName(0, "ico_backup.png")
        Me.tabImageList.Images.SetKeyName(1, "ico_bootstrap.gif")
        Me.tabImageList.Images.SetKeyName(2, "ico_process.png")
        Me.tabImageList.Images.SetKeyName(3, "ico_settings.png")
        Me.tabImageList.Images.SetKeyName(4, "ico_log.png")
        '
        'lstSmallIcons
        '
        Me.lstSmallIcons.ImageStream = CType(resources.GetObject("lstSmallIcons.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.lstSmallIcons.TransparentColor = System.Drawing.Color.Transparent
        Me.lstSmallIcons.Images.SetKeyName(0, "img_app_burn-sml.png")
        Me.lstSmallIcons.Images.SetKeyName(1, "img_app_cd-sml.png")
        Me.lstSmallIcons.Images.SetKeyName(2, "img_app_digital-sml.png")
        Me.lstSmallIcons.Images.SetKeyName(3, "img_app_ruby-sml.png")
        Me.lstSmallIcons.Images.SetKeyName(4, "img_app_trash-sml.png")
        Me.lstSmallIcons.Images.SetKeyName(5, "img_app_unknown-sml.png")
        Me.lstSmallIcons.Images.SetKeyName(6, "img_app_xml-sml.png")
        Me.lstSmallIcons.Images.SetKeyName(7, "img_archive-sml.png")
        Me.lstSmallIcons.Images.SetKeyName(8, "img_dos_exec-sml.png")
        Me.lstSmallIcons.Images.SetKeyName(9, "img_xhtml_xml-sml.png")
        Me.lstSmallIcons.Images.SetKeyName(10, "ico_file.png")
        '
        'tabSettings
        '
        Me.tabSettings.Controls.Add(Me.grpAgent)
        Me.tabSettings.Controls.Add(Me.grpAbout)
        Me.tabSettings.ImageIndex = 3
        Me.tabSettings.Location = New System.Drawing.Point(4, 23)
        Me.tabSettings.Name = "tabSettings"
        Me.tabSettings.Size = New System.Drawing.Size(708, 292)
        Me.tabSettings.TabIndex = 4
        Me.tabSettings.Text = "Settings"
        Me.tabSettings.UseVisualStyleBackColor = True
        '
        'grpAgent
        '
        Me.grpAgent.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grpAgent.Controls.Add(Me.btnImportConfig)
        Me.grpAgent.Controls.Add(Me.btnExportConfig)
        Me.grpAgent.Controls.Add(Me.btnExit)
        Me.grpAgent.Controls.Add(Me.chkShowAgent)
        Me.grpAgent.Controls.Add(Me.chkStartBar)
        Me.grpAgent.Location = New System.Drawing.Point(8, 3)
        Me.grpAgent.Name = "grpAgent"
        Me.grpAgent.Size = New System.Drawing.Size(513, 281)
        Me.grpAgent.TabIndex = 1
        Me.grpAgent.TabStop = False
        '
        'btnImportConfig
        '
        Me.btnImportConfig.Image = Global.ProcessMinder.My.Resources.Resources.ico_import
        Me.btnImportConfig.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnImportConfig.Location = New System.Drawing.Point(178, 62)
        Me.btnImportConfig.Name = "btnImportConfig"
        Me.btnImportConfig.Size = New System.Drawing.Size(166, 23)
        Me.btnImportConfig.TabIndex = 5
        Me.btnImportConfig.Text = "Import Processes"
        Me.btnImportConfig.UseVisualStyleBackColor = True
        '
        'btnExportConfig
        '
        Me.btnExportConfig.Image = Global.ProcessMinder.My.Resources.Resources.ico_export
        Me.btnExportConfig.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnExportConfig.Location = New System.Drawing.Point(6, 62)
        Me.btnExportConfig.Name = "btnExportConfig"
        Me.btnExportConfig.Size = New System.Drawing.Size(166, 23)
        Me.btnExportConfig.TabIndex = 4
        Me.btnExportConfig.Text = "Export Processes"
        Me.btnExportConfig.UseVisualStyleBackColor = True
        '
        'btnExit
        '
        Me.btnExit.Location = New System.Drawing.Point(432, 252)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(75, 23)
        Me.btnExit.TabIndex = 3
        Me.btnExit.Text = "Exit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'chkShowAgent
        '
        Me.chkShowAgent.AutoSize = True
        Me.chkShowAgent.Checked = Global.ProcessMinder.My.MySettings.Default.settingShowWindow
        Me.chkShowAgent.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkShowAgent.DataBindings.Add(New System.Windows.Forms.Binding("Checked", Global.ProcessMinder.My.MySettings.Default, "settingShowWindow", True, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged))
        Me.chkShowAgent.Location = New System.Drawing.Point(6, 16)
        Me.chkShowAgent.Name = "chkShowAgent"
        Me.chkShowAgent.Size = New System.Drawing.Size(136, 17)
        Me.chkShowAgent.TabIndex = 2
        Me.chkShowAgent.Text = "Show Agent on Startup"
        Me.chkShowAgent.UseVisualStyleBackColor = True
        '
        'chkStartBar
        '
        Me.chkStartBar.AutoSize = True
        Me.chkStartBar.Checked = Global.ProcessMinder.My.MySettings.Default.settingShowInTaskBar
        Me.chkStartBar.DataBindings.Add(New System.Windows.Forms.Binding("Checked", Global.ProcessMinder.My.MySettings.Default, "settingShowInTaskBar", True, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged))
        Me.chkStartBar.Location = New System.Drawing.Point(6, 39)
        Me.chkStartBar.Name = "chkStartBar"
        Me.chkStartBar.Size = New System.Drawing.Size(112, 17)
        Me.chkStartBar.TabIndex = 1
        Me.chkStartBar.Text = "Show on Start Bar"
        Me.chkStartBar.UseVisualStyleBackColor = True
        '
        'grpAbout
        '
        Me.grpAbout.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grpAbout.Controls.Add(Me.LinkLabel1)
        Me.grpAbout.Controls.Add(Me.PictureBox1)
        Me.grpAbout.Controls.Add(Me.Label4)
        Me.grpAbout.Controls.Add(Me.Label3)
        Me.grpAbout.Location = New System.Drawing.Point(527, 3)
        Me.grpAbout.Name = "grpAbout"
        Me.grpAbout.Size = New System.Drawing.Size(173, 281)
        Me.grpAbout.TabIndex = 0
        Me.grpAbout.TabStop = False
        Me.grpAbout.Text = "About"
        '
        'LinkLabel1
        '
        Me.LinkLabel1.AutoSize = True
        Me.LinkLabel1.Location = New System.Drawing.Point(2, 53)
        Me.LinkLabel1.Name = "LinkLabel1"
        Me.LinkLabel1.Size = New System.Drawing.Size(161, 13)
        Me.LinkLabel1.TabIndex = 7
        Me.LinkLabel1.TabStop = True
        Me.LinkLabel1.Text = "http://www.biggerconcept.com/" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = Global.ProcessMinder.My.Resources.Resources.ico_mind
        Me.PictureBox1.Location = New System.Drawing.Point(126, 16)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(37, 34)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 6
        Me.PictureBox1.TabStop = False
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.Black
        Me.Label4.Location = New System.Drawing.Point(6, 34)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(34, 13)
        Me.Label4.TabIndex = 5
        Me.Label4.Text = "Agent"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.Black
        Me.Label3.Location = New System.Drawing.Point(6, 16)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(103, 18)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Process Minder"
        '
        'tabProcesses
        '
        Me.tabProcesses.Controls.Add(Me.ProcessTabSplit)
        Me.tabProcesses.Controls.Add(Me.tolBootstrap)
        Me.tabProcesses.ImageKey = "ico_bootstrap.gif"
        Me.tabProcesses.Location = New System.Drawing.Point(4, 23)
        Me.tabProcesses.Name = "tabProcesses"
        Me.tabProcesses.Size = New System.Drawing.Size(708, 292)
        Me.tabProcesses.TabIndex = 5
        Me.tabProcesses.Text = "Processes"
        Me.tabProcesses.UseVisualStyleBackColor = True
        '
        'lstProcesses
        '
        Me.lstProcesses.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colName, Me.colDescription})
        Me.lstProcesses.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lstProcesses.LargeImageList = Me.lstLargeIcons
        Me.lstProcesses.Location = New System.Drawing.Point(0, 0)
        Me.lstProcesses.Name = "lstProcesses"
        Me.lstProcesses.Size = New System.Drawing.Size(533, 267)
        Me.lstProcesses.SmallImageList = Me.lstSmallIcons
        Me.lstProcesses.TabIndex = 1
        Me.lstProcesses.UseCompatibleStateImageBehavior = False
        Me.lstProcesses.View = System.Windows.Forms.View.Details
        '
        'colName
        '
        Me.colName.Text = "Name"
        Me.colName.Width = 172
        '
        'colDescription
        '
        Me.colDescription.Text = "Description"
        Me.colDescription.Width = 334
        '
        'lstLargeIcons
        '
        Me.lstLargeIcons.ImageStream = CType(resources.GetObject("lstLargeIcons.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.lstLargeIcons.TransparentColor = System.Drawing.Color.Transparent
        Me.lstLargeIcons.Images.SetKeyName(0, "img_app_burn-lge.png")
        Me.lstLargeIcons.Images.SetKeyName(1, "img_app_cd-lge.png")
        Me.lstLargeIcons.Images.SetKeyName(2, "img_app_digital-lge.png")
        Me.lstLargeIcons.Images.SetKeyName(3, "img_app_ruby-lge.png")
        Me.lstLargeIcons.Images.SetKeyName(4, "img_app_trash-lge.png")
        Me.lstLargeIcons.Images.SetKeyName(5, "img_app_unknown-lge.png")
        Me.lstLargeIcons.Images.SetKeyName(6, "img_app_xml-lge.png")
        Me.lstLargeIcons.Images.SetKeyName(7, "img_archive-lge.png")
        Me.lstLargeIcons.Images.SetKeyName(8, "img_dos_exec-lge.png")
        Me.lstLargeIcons.Images.SetKeyName(9, "img_xhtml_xml-lge.png")
        Me.lstLargeIcons.Images.SetKeyName(10, "ico_beard.png")
        '
        'pnlInfo
        '
        Me.pnlInfo.BackColor = System.Drawing.SystemColors.Control
        Me.pnlInfo.Controls.Add(Me.pnlPreview)
        Me.pnlInfo.Controls.Add(Me.lblDescriptionHeading)
        Me.pnlInfo.Controls.Add(Me.lblDescription)
        Me.pnlInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlInfo.Name = "pnlInfo"
        Me.pnlInfo.Size = New System.Drawing.Size(171, 267)
        Me.pnlInfo.TabIndex = 2
        '
        'pnlPreview
        '
        Me.pnlPreview.BackColor = System.Drawing.SystemColors.Control
        Me.pnlPreview.Controls.Add(Me.lblIcon)
        Me.pnlPreview.Controls.Add(Me.tabPreview)
        Me.pnlPreview.Controls.Add(Me.lblPreviewName)
        Me.pnlPreview.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlPreview.Location = New System.Drawing.Point(0, 0)
        Me.pnlPreview.Name = "pnlPreview"
        Me.pnlPreview.Size = New System.Drawing.Size(171, 267)
        Me.pnlPreview.TabIndex = 4
        Me.pnlPreview.Visible = False
        '
        'tabPreview
        '
        Me.tabPreview.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.tabPreview.Appearance = System.Windows.Forms.TabAppearance.FlatButtons
        Me.tabPreview.Controls.Add(Me.tabDetails)
        Me.tabPreview.Controls.Add(Me.tabDescription)
        Me.tabPreview.Location = New System.Drawing.Point(0, 30)
        Me.tabPreview.Name = "tabPreview"
        Me.tabPreview.SelectedIndex = 0
        Me.tabPreview.Size = New System.Drawing.Size(175, 237)
        Me.tabPreview.TabIndex = 8
        '
        'tabDetails
        '
        Me.tabDetails.Controls.Add(Me.btnKill2)
        Me.tabDetails.Controls.Add(Me.btnRun2)
        Me.tabDetails.Controls.Add(Me.btnLog2)
        Me.tabDetails.Controls.Add(Me.txtCommand)
        Me.tabDetails.Controls.Add(Me.lblCommand)
        Me.tabDetails.Location = New System.Drawing.Point(4, 25)
        Me.tabDetails.Name = "tabDetails"
        Me.tabDetails.Padding = New System.Windows.Forms.Padding(3)
        Me.tabDetails.Size = New System.Drawing.Size(167, 193)
        Me.tabDetails.TabIndex = 0
        Me.tabDetails.Text = "Details"
        Me.tabDetails.UseVisualStyleBackColor = True
        '
        'btnKill2
        '
        Me.btnKill2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnKill2.Image = Global.ProcessMinder.My.Resources.Resources.ico_exclam
        Me.btnKill2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnKill2.Location = New System.Drawing.Point(9, 164)
        Me.btnKill2.Name = "btnKill2"
        Me.btnKill2.Size = New System.Drawing.Size(150, 23)
        Me.btnKill2.TabIndex = 11
        Me.btnKill2.Text = "Stop Process"
        Me.btnKill2.UseVisualStyleBackColor = True
        '
        'btnRun2
        '
        Me.btnRun2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnRun2.Image = Global.ProcessMinder.My.Resources.Resources.ico_run
        Me.btnRun2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnRun2.Location = New System.Drawing.Point(9, 135)
        Me.btnRun2.Name = "btnRun2"
        Me.btnRun2.Size = New System.Drawing.Size(150, 23)
        Me.btnRun2.TabIndex = 10
        Me.btnRun2.Text = "Run Process"
        Me.btnRun2.UseVisualStyleBackColor = True
        '
        'btnLog2
        '
        Me.btnLog2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnLog2.Image = Global.ProcessMinder.My.Resources.Resources.ico_log_dir
        Me.btnLog2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnLog2.Location = New System.Drawing.Point(9, 106)
        Me.btnLog2.Name = "btnLog2"
        Me.btnLog2.Size = New System.Drawing.Size(150, 23)
        Me.btnLog2.TabIndex = 9
        Me.btnLog2.Text = "Log Directory"
        Me.btnLog2.UseVisualStyleBackColor = True
        '
        'txtCommand
        '
        Me.txtCommand.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtCommand.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtCommand.Location = New System.Drawing.Point(9, 20)
        Me.txtCommand.Multiline = True
        Me.txtCommand.Name = "txtCommand"
        Me.txtCommand.ReadOnly = True
        Me.txtCommand.Size = New System.Drawing.Size(150, 80)
        Me.txtCommand.TabIndex = 8
        '
        'lblCommand
        '
        Me.lblCommand.AutoSize = True
        Me.lblCommand.Location = New System.Drawing.Point(6, 3)
        Me.lblCommand.Name = "lblCommand"
        Me.lblCommand.Size = New System.Drawing.Size(57, 13)
        Me.lblCommand.TabIndex = 7
        Me.lblCommand.Text = "Command:"
        '
        'tabDescription
        '
        Me.tabDescription.Controls.Add(Me.txtDescription)
        Me.tabDescription.Location = New System.Drawing.Point(4, 25)
        Me.tabDescription.Name = "tabDescription"
        Me.tabDescription.Padding = New System.Windows.Forms.Padding(3)
        Me.tabDescription.Size = New System.Drawing.Size(167, 208)
        Me.tabDescription.TabIndex = 1
        Me.tabDescription.Text = "Description"
        Me.tabDescription.UseVisualStyleBackColor = True
        '
        'txtDescription
        '
        Me.txtDescription.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtDescription.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtDescription.Location = New System.Drawing.Point(3, 3)
        Me.txtDescription.Multiline = True
        Me.txtDescription.Name = "txtDescription"
        Me.txtDescription.ReadOnly = True
        Me.txtDescription.Size = New System.Drawing.Size(161, 202)
        Me.txtDescription.TabIndex = 10
        '
        'lblPreviewName
        '
        Me.lblPreviewName.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPreviewName.ForeColor = System.Drawing.Color.Black
        Me.lblPreviewName.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblPreviewName.ImageIndex = 3
        Me.lblPreviewName.Location = New System.Drawing.Point(33, 0)
        Me.lblPreviewName.Name = "lblPreviewName"
        Me.lblPreviewName.Size = New System.Drawing.Size(135, 27)
        Me.lblPreviewName.TabIndex = 5
        Me.lblPreviewName.Text = "Process Name"
        Me.lblPreviewName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblDescriptionHeading
        '
        Me.lblDescriptionHeading.AutoSize = True
        Me.lblDescriptionHeading.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDescriptionHeading.ForeColor = System.Drawing.Color.Black
        Me.lblDescriptionHeading.Location = New System.Drawing.Point(6, 9)
        Me.lblDescriptionHeading.Name = "lblDescriptionHeading"
        Me.lblDescriptionHeading.Size = New System.Drawing.Size(119, 18)
        Me.lblDescriptionHeading.TabIndex = 3
        Me.lblDescriptionHeading.Text = "Managed Services"
        '
        'lblDescription
        '
        Me.lblDescription.Location = New System.Drawing.Point(6, 42)
        Me.lblDescription.Name = "lblDescription"
        Me.lblDescription.Size = New System.Drawing.Size(157, 179)
        Me.lblDescription.TabIndex = 0
        Me.lblDescription.Text = resources.GetString("lblDescription.Text")
        '
        'tolBootstrap
        '
        Me.tolBootstrap.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btnAddProcess, Me.btnRemoveProcess, Me.btnModify, Me.btnRun, Me.btnKill, Me.btnView, Me.ToolStripSeparator2, Me.btnOpenLogDir})
        Me.tolBootstrap.Location = New System.Drawing.Point(0, 0)
        Me.tolBootstrap.Name = "tolBootstrap"
        Me.tolBootstrap.Size = New System.Drawing.Size(708, 25)
        Me.tolBootstrap.TabIndex = 0
        Me.tolBootstrap.Text = "ToolStrip4"
        '
        'btnAddProcess
        '
        Me.btnAddProcess.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnAddProcess.Image = Global.ProcessMinder.My.Resources.Resources.ico_add
        Me.btnAddProcess.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnAddProcess.Name = "btnAddProcess"
        Me.btnAddProcess.Size = New System.Drawing.Size(23, 22)
        Me.btnAddProcess.Text = "Add"
        '
        'btnRemoveProcess
        '
        Me.btnRemoveProcess.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnRemoveProcess.Enabled = False
        Me.btnRemoveProcess.Image = Global.ProcessMinder.My.Resources.Resources.ico_remove
        Me.btnRemoveProcess.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnRemoveProcess.Name = "btnRemoveProcess"
        Me.btnRemoveProcess.Size = New System.Drawing.Size(23, 22)
        Me.btnRemoveProcess.Text = "Remove"
        '
        'btnModify
        '
        Me.btnModify.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnModify.Enabled = False
        Me.btnModify.Image = Global.ProcessMinder.My.Resources.Resources.ico_edit
        Me.btnModify.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnModify.Name = "btnModify"
        Me.btnModify.Size = New System.Drawing.Size(23, 22)
        Me.btnModify.Text = "Modify"
        '
        'btnRun
        '
        Me.btnRun.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnRun.Enabled = False
        Me.btnRun.Image = Global.ProcessMinder.My.Resources.Resources.ico_start
        Me.btnRun.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnRun.Name = "btnRun"
        Me.btnRun.Size = New System.Drawing.Size(23, 22)
        Me.btnRun.Text = "Run"
        '
        'btnKill
        '
        Me.btnKill.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnKill.Enabled = False
        Me.btnKill.Image = Global.ProcessMinder.My.Resources.Resources.ico_exclam
        Me.btnKill.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnKill.Name = "btnKill"
        Me.btnKill.Size = New System.Drawing.Size(23, 22)
        Me.btnKill.Text = "Kill Process"
        '
        'btnView
        '
        Me.btnView.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.btnView.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnView.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolbarToolStripMenuItem, Me.ProcessListToolStripMenuItem})
        Me.btnView.Image = Global.ProcessMinder.My.Resources.Resources.ico_view
        Me.btnView.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnView.Name = "btnView"
        Me.btnView.Size = New System.Drawing.Size(29, 22)
        Me.btnView.Text = "View"
        '
        'ToolbarToolStripMenuItem
        '
        Me.ToolbarToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.IconsOnlyToolStripMenuItem, Me.IconsAndTextToolStripMenuItem, Me.TextOnlyToolStripMenuItem})
        Me.ToolbarToolStripMenuItem.Name = "ToolbarToolStripMenuItem"
        Me.ToolbarToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.ToolbarToolStripMenuItem.Text = "Toolbar"
        '
        'IconsOnlyToolStripMenuItem
        '
        Me.IconsOnlyToolStripMenuItem.Name = "IconsOnlyToolStripMenuItem"
        Me.IconsOnlyToolStripMenuItem.Size = New System.Drawing.Size(150, 22)
        Me.IconsOnlyToolStripMenuItem.Text = "Icons Only"
        '
        'IconsAndTextToolStripMenuItem
        '
        Me.IconsAndTextToolStripMenuItem.Name = "IconsAndTextToolStripMenuItem"
        Me.IconsAndTextToolStripMenuItem.Size = New System.Drawing.Size(150, 22)
        Me.IconsAndTextToolStripMenuItem.Text = "Icons and Text"
        '
        'TextOnlyToolStripMenuItem
        '
        Me.TextOnlyToolStripMenuItem.Name = "TextOnlyToolStripMenuItem"
        Me.TextOnlyToolStripMenuItem.Size = New System.Drawing.Size(150, 22)
        Me.TextOnlyToolStripMenuItem.Text = "Text Only"
        '
        'ProcessListToolStripMenuItem
        '
        Me.ProcessListToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DetailViewToolStripMenuItem, Me.LargeIconsToolStripMenuItem, Me.SmallIconsToolStripMenuItem, Me.ListToolStripMenuItem, Me.TileToolStripMenuItem, Me.ToolStripSeparator1, Me.SortAZToolStripMenuItem, Me.SortZAToolStripMenuItem})
        Me.ProcessListToolStripMenuItem.Name = "ProcessListToolStripMenuItem"
        Me.ProcessListToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.ProcessListToolStripMenuItem.Text = "Process List"
        '
        'DetailViewToolStripMenuItem
        '
        Me.DetailViewToolStripMenuItem.Name = "DetailViewToolStripMenuItem"
        Me.DetailViewToolStripMenuItem.Size = New System.Drawing.Size(134, 22)
        Me.DetailViewToolStripMenuItem.Text = "Detail View"
        '
        'LargeIconsToolStripMenuItem
        '
        Me.LargeIconsToolStripMenuItem.Name = "LargeIconsToolStripMenuItem"
        Me.LargeIconsToolStripMenuItem.Size = New System.Drawing.Size(134, 22)
        Me.LargeIconsToolStripMenuItem.Text = "Large Icons"
        '
        'SmallIconsToolStripMenuItem
        '
        Me.SmallIconsToolStripMenuItem.Name = "SmallIconsToolStripMenuItem"
        Me.SmallIconsToolStripMenuItem.Size = New System.Drawing.Size(134, 22)
        Me.SmallIconsToolStripMenuItem.Text = "Small Icons"
        '
        'ListToolStripMenuItem
        '
        Me.ListToolStripMenuItem.Name = "ListToolStripMenuItem"
        Me.ListToolStripMenuItem.Size = New System.Drawing.Size(134, 22)
        Me.ListToolStripMenuItem.Text = "List"
        '
        'TileToolStripMenuItem
        '
        Me.TileToolStripMenuItem.Name = "TileToolStripMenuItem"
        Me.TileToolStripMenuItem.Size = New System.Drawing.Size(134, 22)
        Me.TileToolStripMenuItem.Text = "Tile"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(131, 6)
        '
        'SortAZToolStripMenuItem
        '
        Me.SortAZToolStripMenuItem.Name = "SortAZToolStripMenuItem"
        Me.SortAZToolStripMenuItem.Size = New System.Drawing.Size(134, 22)
        Me.SortAZToolStripMenuItem.Text = "Sort A-Z"
        '
        'SortZAToolStripMenuItem
        '
        Me.SortZAToolStripMenuItem.Name = "SortZAToolStripMenuItem"
        Me.SortZAToolStripMenuItem.Size = New System.Drawing.Size(134, 22)
        Me.SortZAToolStripMenuItem.Text = "Sort Z-A"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'btnOpenLogDir
        '
        Me.btnOpenLogDir.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnOpenLogDir.Enabled = False
        Me.btnOpenLogDir.Image = Global.ProcessMinder.My.Resources.Resources.ico_log_dir
        Me.btnOpenLogDir.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnOpenLogDir.Name = "btnOpenLogDir"
        Me.btnOpenLogDir.Size = New System.Drawing.Size(23, 22)
        Me.btnOpenLogDir.Text = "Open Log"
        '
        'tabs
        '
        Me.tabs.Controls.Add(Me.tabProcesses)
        Me.tabs.Controls.Add(Me.tabSettings)
        Me.tabs.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tabs.ImageList = Me.tabImageList
        Me.tabs.Location = New System.Drawing.Point(0, 65)
        Me.tabs.Name = "tabs"
        Me.tabs.SelectedIndex = 0
        Me.tabs.Size = New System.Drawing.Size(716, 319)
        Me.tabs.TabIndex = 2
        '
        'tray
        '
        Me.tray.ContextMenuStrip = Me.trayMenu
        Me.tray.Icon = CType(resources.GetObject("tray.Icon"), System.Drawing.Icon)
        Me.tray.Text = "ProcessMinder"
        Me.tray.Visible = True
        '
        'trayMenu
        '
        Me.trayMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.trayBtnAdd, Me.trayBtnManage, Me.TrayExit})
        Me.trayMenu.Name = "trayMenu"
        Me.trayMenu.Size = New System.Drawing.Size(172, 70)
        '
        'trayBtnAdd
        '
        Me.trayBtnAdd.Image = Global.ProcessMinder.My.Resources.Resources.ico_add
        Me.trayBtnAdd.Name = "trayBtnAdd"
        Me.trayBtnAdd.Size = New System.Drawing.Size(171, 22)
        Me.trayBtnAdd.Text = "Add Process"
        '
        'trayBtnManage
        '
        Me.trayBtnManage.Image = Global.ProcessMinder.My.Resources.Resources.ico_bootstrap
        Me.trayBtnManage.Name = "trayBtnManage"
        Me.trayBtnManage.Size = New System.Drawing.Size(171, 22)
        Me.trayBtnManage.Text = "Manage Processes"
        '
        'TrayExit
        '
        Me.TrayExit.Name = "TrayExit"
        Me.TrayExit.Size = New System.Drawing.Size(171, 22)
        Me.TrayExit.Text = "Exit"
        '
        'lblIcon
        '
        Me.lblIcon.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIcon.ForeColor = System.Drawing.Color.Black
        Me.lblIcon.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblIcon.ImageIndex = 3
        Me.lblIcon.ImageList = Me.lstSmallIcons
        Me.lblIcon.Location = New System.Drawing.Point(6, 0)
        Me.lblIcon.Name = "lblIcon"
        Me.lblIcon.Size = New System.Drawing.Size(21, 27)
        Me.lblIcon.TabIndex = 9
        Me.lblIcon.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'ProcessTabSplit
        '
        Me.ProcessTabSplit.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ProcessTabSplit.Location = New System.Drawing.Point(0, 25)
        Me.ProcessTabSplit.Name = "ProcessTabSplit"
        '
        'ProcessTabSplit.Panel1
        '
        Me.ProcessTabSplit.Panel1.Controls.Add(Me.lstProcesses)
        '
        'ProcessTabSplit.Panel2
        '
        Me.ProcessTabSplit.Panel2.Controls.Add(Me.pnlInfo)
        Me.ProcessTabSplit.Size = New System.Drawing.Size(708, 267)
        Me.ProcessTabSplit.SplitterDistance = 533
        Me.ProcessTabSplit.TabIndex = 3
        '
        'MainView
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(716, 384)
        Me.Controls.Add(Me.tabs)
        Me.Controls.Add(Me.pnlTitle)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "MainView"
        Me.Text = "Process Minder :: Agent"
        Me.pnlTitle.ResumeLayout(False)
        Me.pnlTitle.PerformLayout()
        CType(Me.pctMap, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabSettings.ResumeLayout(False)
        Me.grpAgent.ResumeLayout(False)
        Me.grpAgent.PerformLayout()
        Me.grpAbout.ResumeLayout(False)
        Me.grpAbout.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabProcesses.ResumeLayout(False)
        Me.tabProcesses.PerformLayout()
        Me.pnlInfo.ResumeLayout(False)
        Me.pnlInfo.PerformLayout()
        Me.pnlPreview.ResumeLayout(False)
        Me.tabPreview.ResumeLayout(False)
        Me.tabDetails.ResumeLayout(False)
        Me.tabDetails.PerformLayout()
        Me.tabDescription.ResumeLayout(False)
        Me.tabDescription.PerformLayout()
        Me.tolBootstrap.ResumeLayout(False)
        Me.tolBootstrap.PerformLayout()
        Me.tabs.ResumeLayout(False)
        Me.trayMenu.ResumeLayout(False)
        Me.ProcessTabSplit.Panel1.ResumeLayout(False)
        Me.ProcessTabSplit.Panel2.ResumeLayout(False)
        CType(Me.ProcessTabSplit, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ProcessTabSplit.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlTitle As System.Windows.Forms.Panel
    Friend WithEvents pctMap As System.Windows.Forms.PictureBox
    Friend WithEvents pnlAppTitle As System.Windows.Forms.Label
    Friend WithEvents tabImageList As System.Windows.Forms.ImageList
    Friend WithEvents lstSmallIcons As System.Windows.Forms.ImageList
    Friend WithEvents tabSettings As System.Windows.Forms.TabPage
    Friend WithEvents grpAgent As System.Windows.Forms.GroupBox
    Friend WithEvents grpAbout As System.Windows.Forms.GroupBox
    Friend WithEvents LinkLabel1 As System.Windows.Forms.LinkLabel
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents tabProcesses As System.Windows.Forms.TabPage
    Friend WithEvents lstProcesses As System.Windows.Forms.ListView
    Friend WithEvents tolBootstrap As System.Windows.Forms.ToolStrip
    Friend WithEvents btnRemoveProcess As System.Windows.Forms.ToolStripButton
    Friend WithEvents tabs As System.Windows.Forms.TabControl
    Friend WithEvents btnAddProcess As System.Windows.Forms.ToolStripButton
    Friend WithEvents colName As System.Windows.Forms.ColumnHeader
    Friend WithEvents colDescription As System.Windows.Forms.ColumnHeader
    Friend WithEvents pnlInfo As System.Windows.Forms.Panel
    Friend WithEvents lblDescription As System.Windows.Forms.Label
    Friend WithEvents lblDescriptionHeading As System.Windows.Forms.Label
    Friend WithEvents btnModify As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnRun As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnView As System.Windows.Forms.ToolStripDropDownButton
    Friend WithEvents ToolbarToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents IconsOnlyToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents IconsAndTextToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TextOnlyToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ProcessListToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DetailViewToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LargeIconsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SmallIconsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents lstLargeIcons As System.Windows.Forms.ImageList
    Friend WithEvents chkStartBar As System.Windows.Forms.CheckBox
    Friend WithEvents ListToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TileToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents SortAZToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SortZAToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tray As System.Windows.Forms.NotifyIcon
    Friend WithEvents trayMenu As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents trayBtnAdd As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents trayBtnManage As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TrayExit As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents chkShowAgent As System.Windows.Forms.CheckBox
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents btnOpenLogDir As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnImportConfig As System.Windows.Forms.Button
    Friend WithEvents btnExportConfig As System.Windows.Forms.Button
    Friend WithEvents btnKill As System.Windows.Forms.ToolStripButton
    Friend WithEvents pnlPreview As System.Windows.Forms.Panel
    Friend WithEvents lblCommand As System.Windows.Forms.Label
    Friend WithEvents lblPreviewName As System.Windows.Forms.Label
    Friend WithEvents tabPreview As System.Windows.Forms.TabControl
    Friend WithEvents tabDetails As System.Windows.Forms.TabPage
    Friend WithEvents btnKill2 As System.Windows.Forms.Button
    Friend WithEvents btnRun2 As System.Windows.Forms.Button
    Friend WithEvents btnLog2 As System.Windows.Forms.Button
    Friend WithEvents txtCommand As System.Windows.Forms.TextBox
    Friend WithEvents tabDescription As System.Windows.Forms.TabPage
    Friend WithEvents txtDescription As System.Windows.Forms.TextBox
    Friend WithEvents lblIcon As System.Windows.Forms.Label
    Friend WithEvents ProcessTabSplit As System.Windows.Forms.SplitContainer

End Class
