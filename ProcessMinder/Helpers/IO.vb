﻿Imports System.IO
Imports System.Xml.Serialization
Imports System.Xml

Module IO

    Private fw As StreamWriter
    Private fr As FileStream

    Public Sub write(ByRef path As String)
        Dim memStrm As MemoryStream = SerializeBinary(ServiceController.Index)
        fw = New StreamWriter(path)

        memStrm.WriteTo(fw.BaseStream)

        memStrm.Close()
        fw.Close()
    End Sub

    Public Sub read(ByRef path As String)
        If System.IO.File.Exists(path) Then
            fr = New FileStream(path, FileMode.Open)
            Dim memStrm As New MemoryStream

            memStrm.SetLength(fr.Length)
            fr.Read(memStrm.GetBuffer, 0, CInt(Fix(fr.Length)))

            Dim fileItems As ArrayList
            fileItems = DeserializeBinary(memStrm)

            For Each item As Service In fileItems
                If ServiceController.Find(item.name).Count = 0 Then
                    ServiceController.Index.Add(item)
                End If
            Next

            memStrm.Close()
            fr.Close()
        End If
    End Sub

    Private Function SerializeBinary(ByVal obj As Object) As System.IO.MemoryStream

        Dim serializer As New System.Runtime.Serialization.Formatters.Binary.BinaryFormatter
        Dim memStrm As New System.IO.MemoryStream
        serializer.Serialize(memStrm, obj)
        Return memStrm

    End Function

    Private Function DeserializeBinary(ByVal memStrm As System.IO.MemoryStream) As Object

        memStrm.Position = 0
        Dim serializer As New System.Runtime.Serialization.Formatters.Binary.BinaryFormatter
        Dim obj As Object = serializer.Deserialize(memStrm)

        Return obj

    End Function

End Module
